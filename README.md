------------
Introduction
------------

Hey, thanks for taking the time to check out my submission. Below are instructions on
how to run the program. I am assuming you have java installed on your command line 
(all Macs should have it pre-installed). 

If this helps, when I run java -version in my Terminal I get this output:

java version "1.6.0_65"
Java(TM) SE Runtime Environment (build 1.6.0_65-b14-462-11M4609)
Java HotSpot(TM) 64-Bit Server VM (build 20.65-b04-462, mixed mode)

I'll also give a high-level intro on how run this in eclipse if you run into issues with the command line.
Before going into the instructions though I want to make some clarifications about how my code is written

--------------
Points to Note
--------------

1. The dataset in the problem I think is in the form of a Ruby-style array.
   Java arrays don't work like this, so I decided to make the dataset a CSV file 
   called "emailPredictor_Dataset.csv" that should be located inside the /src directory.
   If you modify the dataset please ensure to keep the same file structure as my code is 
   written with that deliberate structure in mind. Here it is incase you have the desire
   to modify it and add more emails to the dataset

   [First Name]{space}[Last Name]{comma}[email]{comma}

   NOTE: The last line of the csv file should not have a trailing comma
   NOTE NOTE: A trailing new line [SublimeText likes to do this] in the dataset will break the code

2. I tried not to over comment the code so quick high level explanation. The core of my program works by parsing 
	 the dataset and populating a HashTable with the mappings "email_domain_name" => "prediction_type." That hash
	 table is what drives my email generation process.
 
-------------------
Running the Program
-------------------

- From Command Line

1. Navigate to the /src folder and run the following:

java emailPredictor.EmailPredictor "[NAME]" "[EMAILDOMAIN]"

So an example would be:

java emailPredictor.EmailPredictor "Tiger Woods" "pga.com"

NOTE: Ensure to send the arguments a Strings


- Eclipse

You can download eclipse here:

https://www.eclipse.org/downloads/

1. Import (File -> Import->General->Existing Project Into Workspace) the project 
	 into eclipse at the root folder level (ie the directory that contains /src).
2. On line 85 in the emailPredictor.java file change the value of arg[0] to the name
	 String you want use and arg[1] to the email domain you want to use.
3. Hit the run green arrow and run EmailPredictor.java