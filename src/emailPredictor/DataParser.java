package emailPredictor;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/*
 * Author: Christian Cocking
 * AlphaSights Applcation
 * 
 * The DataParser class reads the dataset (as a CSV file) and converts it to a Map<String, String> 
 */

public class DataParser {

	private final static String filePath = "emailPredictor_Dataset.csv";
	
	/*
	 * This method reads the dataset that is stored in a CSV file and returns
	 * a mapping of domain names -> PredictionTypes
	 * 
	 * So after running this on the provided dataset we get: 
	 * 
	 * {
	 * apple.com=[first_initial_dot_last_initial],
	 * google.com=[first_name_dot_last_initial, first_initial_dot_last_name], 
	 * alphasights.com=[first_name_dot_last_name]
	 * }
	 * 
	 * in a HashTable
	 * 
	 * This information is used by the email predictor class to generate emails based on the input domain name
	 * 
	 */
	
	protected Map<String, Set<PredictionType>> parseEmailDomainsToPredictionType () throws FileNotFoundException
	{
		// Reading the file
		File file = new File(filePath);
		Scanner s = new Scanner(file);
		s.useDelimiter(",");
		Map<String, Set<PredictionType>> map = new Hashtable<String, Set<PredictionType>>();
		
		// Scanning the file and populating the hashtable
		while (s.hasNext())
		{
			// Skip the the name
			s.next();
			String email = s.next();
			String [] split_email = email.split("@");
			if (map.containsKey(split_email[1]))
			{
				Set<PredictionType> predictionList = map.get(split_email[1]);
				predictionList.add(determineEmailPattern(split_email[0]));
				map.put(split_email[1], predictionList);
			} else
			{
				Set<PredictionType> new_predictionList = new TreeSet<PredictionType>();
				new_predictionList.add(determineEmailPattern(split_email[0]));
				map.put(split_email[1], new_predictionList);
			}
		}
		return map;
	}
	
	/*
	 * This helper method is used to determine the pattern (PredictionType) that a certain email is portraying 
	 * 
	 */
	
	
	private PredictionType determineEmailPattern(String email_hostname)
	{
		//Get the first_name and last_name from the email sepeated by the dot. Note the \\ here is used for escaping
		String [] email_split = email_hostname.split("\\.");
		int first_name_length = email_split[0].length();
		int last_name_length = email_split[1].length();
		
		// Based on the length of the above determine the case
		if (first_name_length > 1)
		{
			if (last_name_length > 1) return PredictionType.first_name_dot_last_name;
			else if (last_name_length == 1) return PredictionType.first_name_dot_last_initial;
		} else if (first_name_length == 1)
		{
			if (last_name_length > 1) return PredictionType.first_initial_dot_last_name;
			else if (last_name_length == 1) return PredictionType.first_initial_dot_last_initial;
		}
		// If the email is poorly formatted we return this which will have a future side effect of cuasing no emails to be generated for this domain
		// If the email is properly formatted like the prompt asks this won't happen
		return PredictionType.could_not_determine_structure;	
	}	
}
