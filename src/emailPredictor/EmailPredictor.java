package emailPredictor;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class EmailPredictor {
	
	
	/*
	 * This is the method that should be called to test the functionality of the program
	 */
	public Set<String> predictEmails (String full_name, String domain) throws FileNotFoundException
	{
		// Getting the first and last name based on the input name
		String [] name = full_name.split(" ");
		String first_name = name[0];
		String last_name = name[1];
		// Instantiating a DataParser that will be used to get the domain name -> PredictionType mapping
		DataParser dp = new DataParser();
		Map <String, Set<PredictionType>> prediction_mappings =  dp.parseEmailDomainsToPredictionType();
		// Grab the predictions for that domain and use that to generate the emails
		Set<PredictionType> prediction_type_list =  prediction_mappings.get(domain);
		Set<String> predicted_emails = generatePredictedEmails(first_name, last_name, prediction_type_list, domain);
		
		return predicted_emails;
	}
	
	/*
	 * This method generates emails given a first name, last name, domain name and a Set of prediction formats
	 *
	 * Example Input: "Christian","Cocking",[first_name_dot_last_initial, first_initial_dot_last_name], whitehouse.gov
	 * Example Output: ["c.cocking@whitehouse.gov, christian.c@whitehouse.gov"]
	 */
	private Set<String> generatePredictedEmails (String first_name, String last_name, Set<PredictionType> prediction_formats, String domain)
	{
		Set<String> email_predictions = new TreeSet<String>();
		// If we pass no prediction formats then we must generate emails of all formats
		if (prediction_formats == null)
		{
			// Generate first_name_dot_last_name
			String generated_email1 = first_name.toLowerCase() + "." + last_name.toLowerCase() + "@" + domain;
			email_predictions.add(generated_email1);
			// Generate first_name_dot_last_initial
			String generated_email2 = first_name.toLowerCase() + "." + Character.toLowerCase(last_name.charAt(0)) + "@" + domain;
			email_predictions.add(generated_email2);
			// Generate first_initial_dot_last_initial
			String generated_email3 = Character.toLowerCase(first_name.charAt(0)) + "." + Character.toLowerCase(last_name.charAt(0))+ "@" + domain;
			email_predictions.add(generated_email3);
			// Generate first_initial_dot_last_name
			String generated_email4 = Character.toLowerCase(first_name.charAt(0)) + "." + last_name.toLowerCase() + "@" + domain;
			email_predictions.add(generated_email4);
		} else 
		{	// For each prediction type we generate the correctly formatted email and add it to our output set
			for (PredictionType p : prediction_formats)
			{
				String generated_email = "";
				switch (p)
				{
				case first_name_dot_last_name:
					generated_email = first_name.toLowerCase() + "." + last_name.toLowerCase()+ "@" + domain;
					break;
				case first_name_dot_last_initial:
					generated_email = first_name.toLowerCase() + "." + Character.toLowerCase(last_name.charAt(0)) + "@" + domain;
					break;
				case first_initial_dot_last_initial:
					generated_email = Character.toLowerCase(first_name.charAt(0)) + "." + Character.toLowerCase(last_name.charAt(0))+ "@" + domain;
					break;
				case first_initial_dot_last_name:
					generated_email = Character.toLowerCase(first_name.charAt(0)) + "." + last_name.toLowerCase()+ "@" + domain;
					break;
				default:
					break;
				}
				email_predictions.add(generated_email);
			}
		}
		return email_predictions;
	}
	
	public static void main (String [] args) throws FileNotFoundException
	{
		EmailPredictor ep = new EmailPredictor();
		Set<String> predicted_emails = ep.predictEmails(args[0], args[1]);
		System.out.println(predicted_emails.toString());
	}
}
