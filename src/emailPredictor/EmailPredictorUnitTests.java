package emailPredictor;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Test;

public class EmailPredictorUnitTests {

	@Test
	public void test() throws FileNotFoundException {
		EmailPredictor ep = new EmailPredictor();
		assertEquals(ep.predictEmails("Christian Cocking","google.com").toString(),"[c.cocking@google.com, christian.c@google.com]");
		assertEquals(ep.predictEmails("Marti Dumas","alphasights.com").toString(),"[marti.dumas@alphasights.com]");
		assertEquals(ep.predictEmails("Steve Jobs","apple.com").toString(),"[s.j@apple.com]");
		assertEquals(ep.predictEmails("Barack Obama","whitehouse.gov").toString(),"[b.o@whitehouse.gov, b.obama@whitehouse.gov, barack.o@whitehouse.gov, barack.obama@whitehouse.gov]");
	}

}
