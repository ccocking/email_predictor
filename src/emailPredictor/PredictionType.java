package emailPredictor;

	// Created an Enum to help keep track of the Prediction Types
	public enum PredictionType {
		first_name_dot_last_name,
		first_name_dot_last_initial,
		first_initial_dot_last_name,
		first_initial_dot_last_initial,
		could_not_determine_structure
}
